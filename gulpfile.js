'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
const concat = require('gulp-concat')
 
sass.compiler = require('node-sass');
 
gulp.task('sass', function () {
  return gulp.src('./sass/*.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
 
gulp.task('sass-all-compiler', function () {
  return gulp.src('./sass/*.sass')
    .pipe(concat('main.sass'))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
gulp.task('sass-all-compiler-minify', function () {
  return gulp.src('./sass/*.sass')
    .pipe(concat('main-min.sass'))
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
gulp.task('sass:watch', function () {
  gulp.watch('./sass/*.sass', gulp.series('sass-all-compiler-minify'));
});
